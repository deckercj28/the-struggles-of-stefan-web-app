# The Struggles Of Stefan Web App

Currently the app is not compatible with mobile browsers.

## Getting started

1. Build the WebGL platform from Unity to the public folder in a SPA
2. Either replace the SPA's root index.html with the WebGL's index.html, or create a component to call with the WebGL's index.html
3. Make sure a folder called `StreamingAssets` with videos in there is at the root of the project
4. Make sure everything runs well with `npm run dev`
5. Run `npm run build`
6. Move the `StreamingAssets` folder into the newly created dist folder
7. Make sure the dist folder runs well by running `npm run preview`
8. Move that folder to this repository and rename dist to `public` 
